# encoding:utf-8
import os
import pandas as pd
from flask import render_template
from flask_login import login_required
from flask_login import current_user
from flask import request
from werkzeug.exceptions import abort
from apps import config
from apps.blueprint import view
from apps.user.process.permissions import permission_required
import util.xtool as xtool
# import sys
# sys.path.append('..')
import xconfig as C
# import re
import util.globalvar as glv


@view.route('/', methods=['GET', 'POST'])
def index():
    return render_template('/index.html')


@view.route('/NoPermission', methods=['GET', 'POST'])
def noPermission():
    # return render_template('index.html')
    return ' NoPermission'


@view.route('/tv', methods=['GET', 'POST'])
@login_required
def tv():
   
    alert_tbl = ''
    top_att_tbl = ''
    top_cp_tbl = ''
    notes = None
    all_pattern = ''
    blk_pattern = ''
    cod_pattern = ''
    return render_template('/tv.html',
                               alert_tbl=alert_tbl,
                               top_att_tbl=top_att_tbl,
                               top_cp_tbl=top_cp_tbl,
                               host=C.tvBackend_host,
                               addWindow=C.addWindow,
                               notes=notes,
                               all_pattern=all_pattern,
                               blk_pattern=blk_pattern,
                               cod_pattern=cod_pattern
                               )
  

# 通用视图函数
@view.route('/<path:path>', methods=['GET', 'POST'])
def pages(path):
    absolute_path = os.path.abspath("{}/{}/{}.html".format(os.path.dirname(__file__), view.template_folder, path))
    if not os.path.isfile(absolute_path):
        abort(404)
    return render_template('{}.html'.format(path))


# 所有以user开头的页面都会跑这里了
@view.route('/user/<path:path>', methods=['GET', 'POST'])
@login_required
def user_pages(path):
    path = "/user/{}".format(path)
    absolute_path = os.path.abspath("{}/{}/{}.html".format(os.path.dirname(__file__), view.template_folder, path))
    if not os.path.isfile(absolute_path):
        abort(404)
    return render_template('{}.html'.format(path))


# 所有以admin开头的页面都会跑这里了
# permission_required限制至少Admin以上权重的用户才能访问
@view.route('/admin/<path:path>', methods=['GET', 'POST'])
@login_required
@permission_required(config.Role.ADMIN)
def admin_pages(path):
    path = "/admin/{}".format(path)
    absolute_path = os.path.abspath("{}/{}/{}.html".format(os.path.dirname(__file__), view.template_folder, path))
    if not os.path.isfile(absolute_path):
        abort(404)
    return render_template('{}.html'.format(path))
