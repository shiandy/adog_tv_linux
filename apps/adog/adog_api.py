# encoding:utf-8

import re
from apps.blueprint import adog_api
from flask_login import current_user, login_user, logout_user, login_required
import xconfig as appc
import socket
from flask import Flask, send_from_directory
from flask import request
from flask import render_template
from flask import json
import calendar
        
import util.globalvar as glv
global dataMan
# dataMan=DM.DataMan()

calendar.setfirstweekday(firstweekday=6)
week = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
socketIOInfo = {
              'name_space':appc.name_space,
              'host':appc.host,
              'port':appc.socketIO_port
              }
# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
# socketio = SocketIO(app, async_mode=async_mode)


@adog_api.route('/noPermission')
def noPermission():
    return render_template('/adog/noPermission.html')



@adog_api.route('/public/contact', methods=['GET', 'POST'])
def contact():
    
    return render_template('/public/contact.html')
    
