# encoding:utf-8
from flask import Blueprint
__author__ = 'adog'

'''
蓝本
'''
# 视图路由
view = Blueprint('view', __name__, template_folder="templates", static_url_path='/static', static_folder='static')

# api路由
api = Blueprint('api', __name__)

# api路由
adog_api = Blueprint('adog_api', __name__)

'''
将需要用到路由的文件都导入到下面,必须是下面
'''

from apps import views
from apps.user.apis import user
from apps.adog.adog_api import *

